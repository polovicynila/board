const board = document.getElementById("board")
const SQUAERS_NUMBER = 1000
const colors = [
    '#48C9B0',
    '#EB984E',
    '#D98880',
    '#52BE80',
    '#52BE80',
    '#2ECC71',
    '#3498DB',
    '#D35400',
    '#C0392B',
    '#F1C40F',

]

for(let i = 0; i < SQUAERS_NUMBER; i++){
    const square = document.createElement('div')
    square.classList.add('square')
    square.addEventListener('mouseover', () => {
        setColor(square)
    })
    square.addEventListener('mouseleave', () => {
        removeColor(square)
    })
    board.append(square)

}
function setColor(element){
    const color = getRandomColor()
    element.style.background = color
}

function removeColor(element){
    element.style.background = 'rgb(36, 36, 36)'
}


function getRandomColor(){
    let colorRandom = Math.floor(Math.random()*colors.length);
    return colors[colorRandom]
}